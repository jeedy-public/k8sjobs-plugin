#!/bin/bash

#Create a temp directory in which we will build the yaml files based on the json the user provides
mkdir -p ./temp

#Create a kustomization.yaml file in which we will put all the resources we want to deploy
touch kustomization.yaml
kustomize edit fix >/dev/null 2>&1

#Use username and UID based on what's in the system-config directory
if [[ -d system-config ]]; then
	if [[ -f "system-config/config.json" ]]; then
		USER_ACCOUNT=$(jq -r '.user' system-config/config.json)
		SA_ID=$(jq -r '.uid' system-config/config.json)
	fi
fi

#Start scanning all jobs the user wants to deploy and create yaml files in our temp directory
for jobsFile in json/*.json; do
    for k in $(jq -r '.jobs[].name' "$jobsFile"); do

        newSchedule=$(jq -r --arg JOBNAME "$k" '.jobs[] | select(.name==$JOBNAME) | .schedule' "$jobsFile")
        [[ "$newSchedule" == "null" ]] && { echo "ERROR: Can't find schedule field on job $k in the $jobsFile file, the script will now exit."; exit 1; }

        newSuspended=$(jq -r --arg JOBNAME "$k" '.jobs[] | select(.name==$JOBNAME) | .suspend' "$jobsFile")
        [[ "$newSuspended" == "null" ]] && { echo "ERROR: Can't find suspend field on job $k in the $jobsFile file, the script will now exit."; exit 1; }

        newDeadline=$(jq -r --arg JOBNAME "$k" '.jobs[] | select(.name==$JOBNAME) | .deadline' "$jobsFile")
        [[ "$newDeadline" == "null" ]] && { echo "ERROR: Can't find deadline field on job $k in the $jobsFile file, the script will now exit."; exit 1; }

        newImage=$(jq -r --arg JOBNAME "$k" '.jobs[] | select(.name==$JOBNAME) | .image' "$jobsFile")
        [[ "$newImage" == "null" ]] && { echo "ERROR: Can't find image field on job $k in the $jobsFile file, the script will now exit."; exit 1; }

        newTemplate=$(jq -r --arg JOBNAME "$k" '.jobs[] | select(.name==$JOBNAME) | .template' "$jobsFile")
        [[ "$newTemplate" == "null" ]] && { echo "ERROR: Can't find template field on job $k in the $jobsFile file, the script will now exit."; exit 1; }
        
	#Copy the template to the temp directory and prepare to overwrite values based on what's in the json file
        cp templates/"$newTemplate" temp/"$k".yaml
	[[ "$?" -ne 0 ]] && { echo "ERROR: Can't find the requested template for job $k in the $jobsFile file, the script will now exit."; exit 1; }

	basenameJobsFile=$(basename $jobsFile)

        #override values
        sed -i 's#PLACEHOLDER_JOB_NAME#'"$k"'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_SCHEDULE#'"$newSchedule"'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_DEADLINE#'"$newDeadline"'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_SUSPENDED#'"$newSuspended"'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_IMAGE#'"$newImage"'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_TEMPLATE#'"$newTemplate"'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_JOBS_FILE#'"$basenameJobsFile"'#' temp/"$k".yaml

	if [[ ! -z "${USER_ACCOUNT}" ]]; then
        sed -i 's#PLACEHOLDER_NFS_CLAIM_NAME#'"$USER_ACCOUNT"-nfs-claim'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_NAMESPACE#'"$USER_ACCOUNT"-ns'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_SA#'"$USER_ACCOUNT"-sa'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_ACCOUNT#'"$USER_ACCOUNT"'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_MASTERSECRET_NAME#'"$USER_ACCOUNT"-master-secret'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_PULL_IMAGE_SECRET#'"$USER_ACCOUNT"-image-puller'#' temp/"$k".yaml
	fi
	
	#If system-config directory exists and is not empty, we has to overwrite the UID we want to run the jobs as
	if [[ -d system-config ]]; then
        	if [[ ! -z "$(ls -A system-config)" ]]; then
                	sed -i 's#PLACEHOLDER_UID#'"$SA_ID"'#' temp/"$k".yaml
                	sed -i 's#PLACEHOLDER_GID#'"$SA_ID"'#' temp/"$k".yaml
        	fi
	fi
 
    done

    #We have to make a configmap out of the json file the user provided
    kustomize edit add configmap "$basenameJobsFile" --from-file=json/"$basenameJobsFile" --disableNameSuffixHash >/dev/null 2>&1

done

kustomize edit add resource temp/* >/dev/null 2>&1
kustomize edit fix >/dev/null 2>&1

kustomize build .

